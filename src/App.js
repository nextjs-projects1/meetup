import { Route, Routes } from "react-router-dom";
import AllMeetupPage from "./pages/AllMeetups";
import NewMeetup from "./pages/NewMeetup";
import Favorites from "./pages/Favorites";
function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<AllMeetupPage/>}  />

        <Route path="/new-meetup" element={<NewMeetup/>} />
        <Route path="/favorites" element={<Favorites/>} />
        
      </Routes>
    </div>
  );
}

export default App;
